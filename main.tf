terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }
  required_version = ">= 1.2.0"
}
provider "aws" {
  region = "eu-west-3"
}
resource "aws_instance" "morning-news-instance" {
  tags={Name="AWS-MORNING-NEWS4"}
  ami           = "ami-0f014d1f920a73971" # Debian 11
  instance_type = "t2.medium" 
  key_name= "my-key-pair"
  # vpc_security_group_ids = [aws_security_group.morning-news-sec-group.id]
}
resource "aws_key_pair" "my_key_pair" {
  key_name   = "my-key-pair"
  public_key = file("/Users/securiteh/my-key-pair.pub") 
}
# description de l'instance AWS EC2 et le groupe de sécurité
resource "aws_security_group" "morning-news-sec-group" {
  description = "groupe projet bash 5 MORNING NEWS"
  name = "morning-news-security-group"
  

  # Règles de trafic entrant
  ingress {
    description = "Allow HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Règles de trafic sortant
  egress {
    description = "Allow OUT"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


