# update your instance ec2
sudo apt-get update
# install prometheus
sudo apt-get install -y prometheus prometheus-node-exporter prometheus-blackbox-exporter
# Démarrez Prometheus
sudo systemctl start prometheus
# Activez Prometheus au démarrage
sudo systemctl enable prometheus
# tester l URL si ça fonctionne
#curl -I http://localhost:9090